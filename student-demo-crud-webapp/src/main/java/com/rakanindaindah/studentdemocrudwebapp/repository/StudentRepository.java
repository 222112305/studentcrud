/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.rakanindaindah.studentdemocrudwebapp.repository;

import com.rakanindaindah.studentdemocrudwebapp.entity.Student;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author DELL
 */
public interface StudentRepository extends JpaRepository<Student, Long> {
    Optional<Student> findByLastName (String lastName);   
}
